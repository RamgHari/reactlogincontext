import React ,{useContext}from 'react'
import AppContext from './context';
const Home = () => {
    const myContext = useContext(AppContext);
    const name = myContext.userData.name;
    return (
        <div>
            <h1>Welcome{name}</h1>
        </div>
     );
}

export default Home;