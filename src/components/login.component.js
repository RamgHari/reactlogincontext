import axios from 'axios';
import React,{useState,useContext} from 'react';
import AppContext from './context';
export default function Login() {
    const [email,setEmail]=useState('')
    const [password,setPassword]=useState('')
    const myContext = useContext(AppContext);
    const pressHandler=async()=>{

        const data = { "email": email,
        "password": password};
        const response = await axios.post('https://reqres.in/api/login', data);
        if(response.status=='200'){
            myContext.setLogged(true)
            myContext.setUserData({name:'Ramji',age:'17'})
        }
    }
    return (
        <form>
            <h3>Sign In</h3>

            <div className="form-group">
                <label>Email address</label>
                <input type="email" className="form-control"  placeholder="Enter email" onChange={(e)=>setEmail(e.target.value)} />
                {email}
            </div>

            <div className="form-group">
                <label>Password</label>
                <input type="password" className="form-control" placeholder="Enter password" onChange={(e)=>setPassword(e.target.value)} />
                {password}

            </div>

            <div className="form-group">
                <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="customCheck1" />
                    <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                </div>
            </div>

            <button type="button" onClick={()=>pressHandler()}  className="btn btn-primary btn-block">Submit</button>
            <p className="forgot-password text-right">
                Forgot <a href="#">password?</a>
            </p>
        </form>
    );
}
